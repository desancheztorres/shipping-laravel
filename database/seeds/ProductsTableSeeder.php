<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create(['name' => 'Provexum R']);
        Product::create(['name' => 'TestoMan R']);
        Product::create(['name' => 'Garcinia R']);
        Product::create(['name' => 'Krygen R']);
        Product::create(['name' => '360Testo']);
        Product::create(['name' => 'XLNan R']);
        Product::create(['name' => 'Keto Plus Pro R']);
        Product::create(['name' => 'Metaboliza R']);
    }
}
