<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use carbon\Carbon;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_id', 11);
            $table->string('trackingNumber', 16)->nullable();
            $table->string('trackingUrl', 100)->nullable();
            $table->string('ipAddress', 15)->nullable();
            $table->enum('status', ['not stated', 'pending', 'processing', 'shipped'])->default('not stated');
            $table->dateTime('ordered_on')->default(Carbon::now());
            $table->datetime('shipped_on')->nullable();

            $table->index(['trackingNumber', 'order_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
