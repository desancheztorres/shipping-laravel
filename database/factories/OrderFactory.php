<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Order;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'order_id' => (String) $faker->numberBetween(0, 70000),
        'trackingNumber' => (String) $faker->numberBetween(1000000000000000, 999999999999999),
        'status' => ['not stated', 'pending', 'processing', 'shipped'][rand(0, 3)],
    ];
});
