<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\{Order, Ship};

$factory->define(Ship::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastname,
        'address1' => $faker->streetName,
        'address2' => $faker->secondaryAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'zip' => $faker->postcode,
        'phone' => $faker->firstName,
        'email' => $faker->email,
        'order_id' => function() {
            return factory(Order::class)->create()->id;
        },
    ];
});
