<?php

    namespace App\Filters;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Http\Request;
    use Illuminate\Support\Arr;

    abstract class FiltersAbstract {
        protected $request;
        protected $filters = [];

        public function __construct(Request $request)
        {
            $this->request = $request;
        }

        public function filter(Builder $builder) {
            foreach ($this->getFilters() as $filter => $class) {
                var_dump($this->resolveFilter($filter));
                die();
            }

            return $builder;
        }

        public function getFilters() {


            $date = [
                'date' => [
                    'from' => $this->filters['from'],
                    'to' => $this->filters['to'],
                ],
            ];

            $filters = array_merge($this->filters, $date);
            dd($filters);

            return $this->filterFilters($filters);

        }

        protected function resolveFilter($filter) {
            return new $this->filters[$filter];
        }

        protected function filterFilters($filters) {
            return $this->request->only(array_keys($this->filters));
        }
    }
