<?php

    namespace App\Filters\Order;

    use App\Filters\FiltersAbstract;

    class OrderFilters extends FiltersAbstract {
        protected $filters = [
            'from' => DateFilter::class,
            'to' => DateFilter::class,
            'orderID' => OrderFilter::class,
        ];
    }
