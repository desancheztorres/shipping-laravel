<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|max:11',
            'trackingNumber' => 'required|max:20',
            'status' => 'in:not stated,pending,processing,shipped',
            'ordered_on' => 'required',
            'shipFirst' => 'required',
            'shipLast' => 'required',
            'shipAddress1' => 'required',
            'shipAddress2' => 'required',
            'shipCity' => 'required',
            'shipState' => 'required',
            'shipZip' => 'required',
            'shipPhone' => 'required',
            'shipEmail' => 'required',
            'billFirst' => 'required',
            'billLast' => 'required',
            'billAddress1' => 'required',
            'billAddress2' => 'required',
            'billCity' => 'required',
            'billState' => 'required',
            'billZip' => 'required',
            'billPhone' => 'required',
            'billEmail' => 'required',
        ];
    }
}
