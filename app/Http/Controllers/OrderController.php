<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\{Order, Product, Ship, Bill};
use Illuminate\Http\Request;
class OrderController extends Controller
{
    public function __call($method, $parameters)
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $status = '') {

        $productsName = array();

        if($request->has('products')) {
            $productsName = Product::select('name')->whereIn('id', $request->get('products'))->get();
        }
        $products = Product::all();
        $allProducts = array();

        foreach ($products as $product) {
            $allProducts[$product->id] = $product->name;
        }

        $filters = [
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'orderID' => $request->get('orderID'),
            'products' => $request->get('products'),
            'trackingNumber' => $request->get('trackingNumber'),
            'shipFirst' => $request->get('shipFirst'),
            'shipLast' => $request->get('shipLast'),
            'shipEmail' => $request->get('shipEmail'),
            'shipAddress1' => $request->get('shipAddress1'),
            'shipAddress2' => $request->get('shipAddress2'),
            'shipCity' => $request->get('shipCity'),
            'shipState' => $request->get('shipState'),
            'shipZip' => $request->get('shipZip'),
            'shipPhone' => $request->get('shipPhone'),
        ];

        $orders = Order::with('ship', 'bill', 'products')->where(function ($query) use ($filters, $status, $productsName) {
            if($status != "") {
                $query->where('status', $status);
            }

            if($filters['from'] && $filters['to']) {
                $query->whereBetween('created_at', array($filters['from'].' 00:00:00', $filters['to'].' 23:59:59'));
            }

            if($filters['orderID']) {
                $query->where('order_id', $filters['orderID']);
            }

            if($filters['trackingNumber']) {
                $query->where('trackingNumber', $filters['trackingNumber']);
            }

            if($filters['shipFirst']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('firstname', $filters['shipFirst']);
                });
            }

            if($filters['shipLast']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('lastname', $filters['shipLast']);
                });
            }

            if($filters['shipAddress1']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('address1', $filters['shipAddress1']);
                });
            }

            if($filters['shipAddress2']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('address2', $filters['shipAddress2']);
                });
            }

            if($filters['shipCity']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('city', $filters['shipCity']);
                });
            }

            if($filters['shipState']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('state', $filters['shipState']);
                });
            }

            if($filters['shipZip']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('zip', $filters['shipZip']);
                });
            }

            if($filters['shipPhone']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('phone', $filters['shipPhone']);
                });
            }

            if($filters['shipEmail']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('email', $filters['shipEmail']);
                });
            }

            if($filters['products']) {
                $query->whereHas('products', function($q) use ($filters, $productsName) {
                    $q->whereIn('name', $productsName);
                });
            }

        })->paginate(10);

        return view('home.orders.index', compact('orders', 'status', 'allProducts'));
    }

    public function create() {

        $products = Product::get();
        $status = ['not stated' => 'Not Stated', 'pending' => 'Pending', 'processing' => 'Processing', 'shipped' => 'Shipped'];

        $allProducts = array();

        foreach ($products as $product) {
            $allProducts[$product->id] = $product->name;
        }

        return view('home.orders.create', compact('allProducts', 'status'));
    }

    protected function getFilters()
    {
        return [
            //
        ];
    }


    public function store(StoreOrderRequest $request) {
        $order = new Order;

        $order->order_id = $request->order_id;
        $order->trackingNumber = $request->trackingNumber;
        $order->status = $request->status;
        $order->ordered_on = date('Y-m-d', strtotime($request->ordered_on));

        $ship = new Ship;
        $ship->firstname = $request->shipFirst;
        $ship->lastname = $request->shipLast;
        $ship->email = $request->shipEmail;
        $ship->phone = $request->shipPhone;
        $ship->state = $request->shipState;
        $ship->address1 = $request->shipAddress1;
        $ship->address2 = $request->shipAddress2;
        $ship->city = $request->shipCity;
        $ship->zip = $request->shipZip;

        $bill = new Bill;
        $bill->firstname = $request->billFirst;
        $bill->lastname = $request->billLast;
        $bill->email = $request->billEmail;
        $bill->phone = $request->billPhone;
        $bill->state = $request->billState;
        $bill->address1 = $request->billAddress1;
        $bill->address2 = $request->billAddress2;
        $bill->city = $request->billCity;
        $bill->zip = $request->billZip;

        $order->save();
        $order->ship()->save($ship);
        $order->bill()->save($bill);

        $order->products()->sync($request->products, false);


        return redirect()->route('orders.index', '')->withSuccess('Order created succesfully.');
    }

    public function edit(Request $request, Order $order) {

        $products = Product::get();
        $status = ['not stated' => 'Not Stated', 'pending' => 'Pending', 'processing' => 'Processing', 'shipped' => 'Shipped'];

        $allProducts = array();

        foreach ($products as $product) {
            $allProducts[$product->id] = $product->name;
        }

        return view('home.orders.edit', compact('order', 'allProducts', 'status'));
    }

    public function update(UpdateOrderRequest $request, Order $order) {

        $order->order_id = $request->get('order_id', $order->order_id);
        $order->trackingNumber = $request->get('trackingNumber', $order->trackingNumber);
        $order->status = $request->get('status', $order->status);
        $order->ordered_on = $request->get('ordered_on', $order->ordered_on);

        $order->ship()->update([
            'firstname' => $request->get('shipFirst', $order->ship->firstname),
            'lastname' => $request->get('shipLast', $order->ship->lastname),
            'email' => $request->get('shipEmail', $order->ship->email),
            'phone' => $request->get('shipPhone', $order->ship->phone),
            'state' => $request->get('shipState', $order->ship->state),
            'address1' => $request->get('shipAddress1', $order->ship->address1),
            'address2' => $request->get('shipAddress2', $order->ship->address2),
            'city' => $request->get('shipZip', $order->ship->city),
            'zip' => $request->get('shipZip', $order->ship->zip),
        ]);

        $order->bill()->update([
            'firstname' => $request->get('billFirst', $order->bill->firstname),
            'lastname' => $request->get('billLast', $order->bill->lastname),
            'email' => $request->get('billEmail', $order->bill->email),
            'phone' => $request->get('billPhone', $order->bill->phone),
            'state' => $request->get('billState', $order->bill->state),
            'address1' => $request->get('billAddress1', $order->bill->address1),
            'address2' => $request->get('billAddress2', $order->bill->address2),
            'city' => $request->get('billZip', $order->bill->city),
            'zip' => $request->get('billZip', $order->bill->zip),
        ]);

        $order->save();
        $order->products()->sync($request->products);

        return redirect()->route('orders.edit', $order->id)->withSuccess('Order updated.');
    }

    public function destroy(Order $order) {
        $order->delete();

        return back()->withSuccess('Order deleted.');
    }

    public function dispatchOrder() {

        return view('home.orders.dispatch');
    }

    public function syncOrder() {

        return view('home.orders.sync');
    }

    public function mergeOrder() {

        return view('home.orders.merge');
    }

}
