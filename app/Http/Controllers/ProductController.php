<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Order, Product};

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::distinct()->get(['products']);
        $prod = [];

        foreach($products as $product){
            array_push($prod, $product->products);
        }

        return $prod;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Request $request)
    {
        $id = $request['id'];
        $order = Order::where('id',$id)->get();
        $products = Product::where('order_id',$order[0]['order_id'])->get();
        $prod = [];

        foreach($products as $product){
            array_push($prod, $product->products);
        }

        return $prod;
    }
}
