<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;

class LogController extends Controller
{
    public function index()
    {

        $logs = Log::all();

        return view('settings.logs', compact('logs'));
    }
}
