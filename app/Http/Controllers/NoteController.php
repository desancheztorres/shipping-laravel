<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use App\InternalLogs\DatabaseLogging;

class NoteController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo 'Create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $note = new Note;
        $note->user_id = $request->user()->id;
        $note->order_id = $request->order_id;
        $note->note = $request->note;
        $note->save();

        $log = new DatabaseLogging();
        $log->logNotes($request->order_id, $request->note, $request->user()->name);

        return back();

    }
}
