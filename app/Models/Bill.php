<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    public $timestamps = false;

    public function order() {
        return $this->belongsTo(Order::class);
    }
}
