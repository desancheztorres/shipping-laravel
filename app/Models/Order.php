<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function ship(){
        return $this->hasOne(Ship::class);
    }

    public function bill(){
        return $this->hasOne(Bill::class);
    }
}
