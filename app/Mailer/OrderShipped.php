<?php

    namespace App\Mailer;

    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use App\Models\Order;

    class OrderShipped extends Mailable
    {
        use Queueable, SerializesModels;

        /**
         * Create a new message instance.
         *
         * @return void
         */
        public function __construct(Order $order, $tracking)
        {
            $this->order = $order;
            $this->tracking = $tracking;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {

            $url = $url = 'https://new.myhermes.co.uk/track.html#/parcel/'.$this->tracking;

            return $this->markdown('emails.orders.shipped')
                ->with(['tracking' => $this->tracking,'url' => $url]);
        }
    }
