@extends('auth.layouts.app')

@section('content')
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <h1>Register</h1>
        <p class="text-muted">Create New Account</p>
        <div class="input-group mb-3">
            <span class="input-group-addon"><i class="fa fa-user w-4"></i></span>
            <input
                type="text"
                class="form-control @error('name') is-invalid @enderror"
                placeholder="Entername"
                name="name"
                id="name"
                value="{{ old('name') }}"
            >
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>
        <div class="input-group mb-4">
            <span class="input-group-addon">
                <i class="fa fa-envelope w-4"></i>
            </span>
            <input
                type="email"
                class="form-control @error('email') is-invalid @enderror"
                placeholder="Enter Email"
                id="email"
                name="email"
                value="{{ old('email') }}"
            >

            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-4">
            <span class="input-group-addon">
                <i class="fa fa-unlock-alt  w-4"></i>
            </span>
            <input
                type="password"
                name="password"
                id="password"
                class="form-control @error('password') is-invalid @enderror"
                placeholder="Password"
            >

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="input-group mb-4">
            <span class="input-group-addon">
                <i class="fa fa-unlock-alt  w-4"></i>
            </span>
            <input
                id="password_confirmation"
                name="password_confirmation"
                type="password"
                class="form-control"
                placeholder="{{ __('Confirm Password') }}"
            >

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-gradient-primary btn-block px-4">{{ __('Register') }}</button>
            </div>
        </div>
    </form>
@endsection
