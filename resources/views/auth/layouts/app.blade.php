@extends('layouts.app')

@section('body_class', 'login-img')

@section('main_content')
    <div class="page-single">
        <div class="container">

            <!-- row -->
            <div class="row">
                <div class="col  mx-auto">
                    <div class="text-center mb-6">
{{--                        <img src="assets/images/brand/logo-light.png" class="" alt="">--}}
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 col-lg-6 col-xl-5 col-sm-7">
                            <div class="card-group mb-0">
                                <div class="card p-4">
                                    <div class="card-body">
                                        @yield('content')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row end -->

        </div>
    </div>
@endsection
