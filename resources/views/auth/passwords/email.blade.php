@extends('auth.layouts.app')

@section('content')
    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <h3>{{ __('Reset Password') }}</h3>
        <div class="input-group mb-4">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input
                type="email"
                class="form-control @error('email') is-invalid @enderror"
                placeholder="Email address"
                name="email"
                value="{{ old('email') }}"
            >
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>
        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-gradient-primary btn-block">{{ __('Send Password Reset Link') }}</button>
            </div>
        </div>
    </form>
@endsection
