@extends('auth.layouts.app')

@section('content')
    <form method="post" action="{{ route('login') }}">
        @csrf
        <h1>{{ __('Login') }}</h1>
        <p class="text-muted">Sign In to your account</p>
        <div class="input-group mb-3">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="email"
                   class="form-control @error('email') is-invalid @enderror"
                   id="email"
                   name="email"
                   value="{{ old('email') }}"
                   placeholder="Enter email"
            >
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-4">
            <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
            <input type="password"
                   class="form-control @error('password') is-invalid @enderror"
                   id="password"
                   name="password"
                   placeholder="Password"
            >
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-4">
            <label class="custom-control custom-checkbox">
                <input
                    type="checkbox"
                    class="custom-control-input"
                    name="remember"
                    id="remember"
                    {{ old('remember') ? 'checked' : '' }}
                />
                <span class="custom-control-label">{{ __('Remember Me') }}</span>
            </label>
        </div>
        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-gradient-primary btn-block">{{ __('Login') }}</button>
            </div>
            @if (Route::has('password.request'))
                <div class="col-12">
                    <a href="{{ route('password.request') }}" class="btn btn-link box-shadow-0 px-0">{{ __('Forgot Your Password?') }}</a>
                </div>
            @endif
        </div>
    </form>
@endsection
