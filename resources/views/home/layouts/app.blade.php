@extends('layouts.app')

@section('body_class', 'app sidebar-mini rtl')

@section('main_content')
    <div class="page-main">

    @include('home.layouts.partials._header')
    @include('home.layouts.partials._sidebar')

    <!-- app-content-->
        <div id="app">
            <div class="app-content  my-3 my-md-5">
                <div class="side-app">

                    @include('home.layouts.partials._page_header')

                    @include('home.layouts.partials.alerts._alerts')
                    <app></app>

                    @yield('content')

                </div>
                @include('home.layouts.partials._footer')
            </div>
        </div>
        <!-- End app-content-->
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}">
{{--    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />--}}
    <style>
        .btn-rounded.btn-sm {
            border-radius: 15px;
            text-align: center;
        }
    </style>

    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

@endsection

@section('js')
    @parent
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
