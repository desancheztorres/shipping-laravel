<div class="alert alert-icon alert-{{ $type }}" role="alert">
    <i class="fa fa-check-circle-o mr-2" aria-hidden="true"></i>
    {{ $slot }}
</div>
