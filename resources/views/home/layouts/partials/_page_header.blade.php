<!-- page-header -->
<div class="page-header">
    <h4 class="page-title">@yield('title')</h4>
    <ol class="breadcrumb"><!-- breadcrumb -->
        <li class="breadcrumb-item"><a href="{{ request()->path() }}">{{ ucfirst(request()->path()) }}</a></li>
{{--        <li class="breadcrumb-item active" aria-current="page">Dashboard 1</li>--}}
    </ol><!-- End breadcrumb -->
</div>
<!-- End page-header -->
