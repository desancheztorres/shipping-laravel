<!--footer-->
<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-12 col-sm-12 text-center">
                Copyright © {{ \Carbon\Carbon::now()->year }}
                <a href="http://www.mdxsolutions.co.uk/" target="_blank">Mdx Solutions</a>.
                Designed by <a href="#">MDX</a>
                All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer-->
