<!--app-header-->
<div class="app-header header d-flex">
    <div class="container-fluid">
        <div class="d-flex">
            <a class="header-brand" href="{{ url('/dashboard') }}">
                {{ config('app.name', 'Shipping') }}
            </a><!-- logo-->
            <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a><!-- sidebar-toggle-->

            <div class="dropdown d-none d-md-flex">
                <a  class="nav-link icon full-screen-link">
                    <i class="fe fe-maximize-2"  id="fullscreen-button"></i>
                </a>
            </div><!-- full-screen -->
            <div class="d-flex order-lg-2 ml-auto horizontal-dropdown">
                <div class="dropdown dropdown-toggle">
                   @auth
                        <a href="#" class="nav-link leading-none" data-toggle="dropdown">
                            <span class="avatar avatar-md brround"><img src="{{ asset('assets/images/faces/male/33.jpg') }}" alt="Profile-img" class="avatar avatar-md brround"></span>
                            <span class="mr-3 d-none d-lg-block ">
                                <span class="text-gray-white"><span class="ml-2">{{ Auth()->user()->name }}</span></span>
                            </span>
                        </a>
                    @endauth
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        @auth
                            <div class="text-center">
                                <a href="#" class="dropdown-item text-center font-weight-sembold user">{{ Auth()->user()->name }}</a>
                                <div class="dropdown-divider"></div>
                            </div>
                        @endauth
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon mdi mdi-account-outline "></i> Profile
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon  mdi mdi-settings"></i> Settings
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                </div><!-- profile -->
            </div>
        </div>
    </div>
</div>
<!--app-header end-->
