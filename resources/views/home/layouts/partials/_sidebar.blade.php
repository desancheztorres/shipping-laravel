<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <div class="dropdown user-pro-body">
            <div>
                <img src="{{ asset('assets/images/faces/male/33.jpg') }}" alt="user-img" class="avatar avatar-xl brround mCS_img_loaded">
            </div>
            @auth
                <div class="user-info">
                    <h4 class="font-weight-semibold text-white mt-2 mb-1">{{ Auth::user()->name }}</h4>
                    <span class="mb-0 text-white-transparent">Mdx Solutions</span>
                </div>
            @endauth
        </div>
    </div>
    <ul class="side-menu">
        @if(Route::has('dashboard'))
        <li>
            <a class="side-menu__item {{ return_if(on_page('dashboard'), 'active') }}"
               href="{{ route('dashboard') }}">
                <i class="side-menu__icon fa fa-question-circle"></i>
                <span class="side-menu__label">Dashboard</span>
            </a>
        </li>
        @endif

        @if(Route::has('orders.index'))

            <li class="slide">
                <a
                    class="side-menu__item {{ return_if(on_page('orders.pending'), 'active')  }}"
                    data-toggle="slide"
                    href="#">
                    <i class="side-menu__icon fa fa-cubes"></i>
                    <span class="side-menu__label">Orders</span>
                    <i class="angle fa fa-angle-right"></i>
                </a>
                <ul class="slide-menu">
                    <li>
                        <a href="{{ route('orders.index', '') }}" class="slide-item">All Orders</a>
                    </li>
                    <li>
                        <a href="{{ route('orders.index', 'processing') }}" class="slide-item">Processing</a>
                    </li>
                    <li>
                        <a href="{{ route('orders.index', 'pending') }}" class="slide-item">Pending</a>
                    </li>
                    <li>
                        <a href="{{ route('orders.index', 'shipped') }}" class="slide-item">Shipped</a>
                    </li>
                    <li>
                        <a href="{{ route('orders.dispatchOrder') }}" class="slide-item">Sync Dispatch</a>
                    </li>
                    <li>
                        <a href="{{ route('orders.syncOrder') }}" class="slide-item">Sync Orders</a>
                    </li>
                    <li>
                        <a href="{{ route('orders.mergeOrder') }}" class="slide-item">Merge Orders</a>
                    </li>
                </ul>
            </li>

        @endif

    </ul>
</aside>
<!--sidemenu end-->
