<div class="row mb-2">
    <div class="col-sm-12 col-lg-4">
        <div class="form-group row">
            <label for="{{ $value }}" class="mr-2 font-weight-bold">{{ $label }}</label>
        </div>

    </div>
    <div class="col-sm-12 col-lg-8">
        <input type="date" class="form-control" name="{{ $value }}" id="{{ $value }}" value="{{ $value }}">
    </div>
</div>
