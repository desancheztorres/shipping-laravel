<div class="col-lg-6 col-xl-3 col-md-6 col-12">
    <div class="card">
        <div class="card-body text-center">
            <div class="">
                <h5>{{ $title }}</h5>
            </div>
            <h2 class="mb-2">
                {{ $slot }}
{{--                <span class="sparkline_bar1 float-right"></span>--}}
            </h2>
{{--            <div><i class="fa fa-arrow-circle-o-up  text-success"></i> 30% Increase</div>--}}
        </div>
    </div>
</div><!-- End col -->
