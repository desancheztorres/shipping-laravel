@extends('home.layouts.app')

@section('content')
    <!-- row -->
    <div class="row">

        @for($i=0; $i<=3; $i++)
            @info_card()
                @slot('title')
                    Total orders
                @endslot
                63321
            @endinfo_card
        @endfor

    </div>
    <!-- End row -->

@endsection
