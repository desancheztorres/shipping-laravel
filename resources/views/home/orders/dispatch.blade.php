@extends('home.layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card overflow-hidden">
                <div class="card-header">
                    <h3 class="card-title">Dispatch Order</h3>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-center mb-4">
                        <input type="text" class="w-15" name="daterange" value="01/01/2018 - 01/15/2018" />
                    </div>

                    <div class="d-flex justify-content-center">
                        <div class="btn btn-primary btn-lg">Generate</div>
                    </div>
                </div>
            </div>

        </div><!-- col end -->
    </div>

@endsection

@section('js')

    @parent

    <script>
        $(function() {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left'
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
    </script>
@endsection
