@extends('home.layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => 'orders.store']) !!}
                @csrf

                @include('home.orders.partials.form')

            {!! Form::close() !!}
            </form>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            $('.basic-multiple').select2();
        });
    </script>

    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection
