@extends('home.layouts.app')

@section('content')
    <app></app>
    <!-- row -->
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-header">
                   <h3 class="card-title">Search Order History</h3>
               </div>
               <div class="card-body">
                   {!! Form::open(['route' => ['orders.index', $status], 'method' => 'GET']) !!}
                        @include('home.orders.partials.search_form')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
    <!-- End row -->

    <div class="row my-5">
        <div class="col">
            <a href="#" class="btn btn-secondary btn-sm text-uppercase btn-rounded mr-2">Generate Labels</a>
            <a href="#" class="btn btn-primary btn-sm text-uppercase btn-rounded mr-2">Generate Dispatch</a>
            <a href="#" class="btn btn-secondary btn-sm text-uppercase btn-rounded mr-2">Mark Shipped</a>
            <a href="#" class="btn btn-primary btn-sm text-uppercase btn-rounded mr-2">Mark Processing</a>
            <a href="#" class="btn btn-secondary btn-sm text-uppercase btn-rounded mr-2">Sync Limelight</a>
            <a href="#" class="btn btn-primary btn-sm text-uppercase btn-rounded mr-2">Refresh dispatch</a>
            <a href="#" class="btn btn-secondary btn-sm text-uppercase btn-rounded mr-2">Force Shipped</a>
            <a href="#" class="btn btn-primary btn-sm text-uppercase btn-rounded mr-2">Re-process</a>
        </div>
    </div>

    <div class="card shadow">
        <div class="card-header">
            <div class="col-8">
                <h2 class="mb-0">Orders</h2>
            </div>
            <div class="col">
                @if(Route::has('orders.create'))
                    <a href="{{ route('orders.create') }}" class="btn btn-primary btn-sm btn-rounded float-right">
                        <i class="fa fa-plus"></i>
                    </a>
                @endif
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">

                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox">
                        </th>
                        <th>Order #</th>
                        <th>Order Date</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Postcode</th>
                        <th>Status</th>
                        <th>Tracking #</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <th>
                                <input type="checkbox" name="" id="">
                            </th>
                            <th scope="row">{{ $order->order_id }}</th>
                            <td>{{ $order->ordered_on }}</td>
                            <td>{{ $order->ship->firstname }} {{ $order->ship->lastname }}</td>
                            <td>{{ $order->ship->email }}</td>
                            <td>{{ $order->ship->zip }}</td>
                            <td>{{ $order->status }}</td>
                            <td>{{ $order->trackingNumber }}</td>
                            <td>
                                <div class="row">
                                    @if(Route::has('orders.edit'))
                                        <div class="col-3">
                                            <button type="button" class="btn btn-secundary btn-sm text-center">
                                                <a href="{{ route('orders.edit', $order->id) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </button>
                                        </div>
                                    @endif

                                    @if(Route::has('orders.delete'))
                                        {!! Form::open(['route' => ['orders.delete', $order->id], 'method' => "POST"]) !!}
                                            @method('DELETE')
                                            @csrf
                                            <div class="col-3">
                                                {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-primary btn-sm text-center'] )  }}
                                            </div>
                                        {!! Form::close() !!}
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <div class="float-right">
                        <span class="font-weight-bold">Total: </span> {{ $orders->total() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="float-right">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@stop

@section('js')
    @parent
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            $('.basic-multiple').select2();
        });
    </script>

    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection


