<div class="card">
    <div class="card-header">
        <h3 class="card-title">Create Order</h3>
    </div>
    <div class="card-body">
        <div id="order-details">
            <div class="row mb-3">
                <div class="col">
                    <h4 class="font-weight-bold">Order Details</h4>
                </div>
            </div>
            <div class="row my-3">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('order_id', 'Order #') !!}
                        {!! Form::text('order_id', null, $attributes = $errors->has('order_id') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('order_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('trackingNumber', 'Tracking #') !!}
                        {!! Form::text('trackingNumber', null, $attributes = $errors->has('trackingNumber') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('trackingNumber')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('status', 'Status') !!}
                        {!! Form::select('status', $status, isset($order) ? $order->status : 'Not stated', array('class' => 'custom-select')) !!}

                        @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        <label class="form-label"></label>
                        {!! Form::label('ordered_on', 'Ordered On') !!}
                        {!! Form::date('ordered_on', isset($order) ? date('Y-m-d', strtotime($order->ordered_on)) : \Carbon\Carbon::now(), $attributes = $errors->has('ordered_on') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('ordered_on')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div id="shipping-details">
            <div class="row mb-3">
                <div class="col">
                    <h4 class="font-weight-bold">Shipping Details</h4>
                </div>
            </div>

            <div class="row my-3">
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('shipFirst', 'Firstname') !!}
                        {!! Form::text('shipFirst', isset($order) ? $order->ship->firstname : null, $attributes = $errors->has('shipFirst') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipFirst')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('shipLast', 'Lastname') !!}
                        {!! Form::text('shipLast', isset($order) ? $order->ship->lastname : null, $attributes = $errors->has('shipLast') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipLast')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('shipEmail', 'Email') !!}
                        {!! Form::text('shipEmail', isset($order) ? $order->ship->email : null, $attributes = $errors->has('shipEmail') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipEmail')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('shipPhone', 'Phone') !!}
                        {!! Form::text('shipPhone', isset($order) ? $order->ship->phone : null, $attributes = $errors->has('shipPhone') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipPhone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="row my-3">
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('shipState', 'State') !!}
                        {!! Form::text('shipState', isset($order) ? $order->ship->state : null, $attributes = $errors->has('shipState') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipState')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('shipAddress1', 'Address 1') !!}
                        {!! Form::text('shipAddress1', isset($order) ? $order->ship->address1 : null, $attributes = $errors->has('shipAddress1') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipAddress1')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="form-group">
                        {!! Form::label('shipAddress2', 'Address 2') !!}
                        {!! Form::text('shipAddress2', isset($order) ? $order->ship->address2 : null, $attributes = $errors->has('shipAddress2') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipAddress2')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="form-group">
                        {!! Form::label('shipCity', 'City') !!}
                        {!! Form::text('shipCity', isset($order) ? $order->ship->city : null, $attributes = $errors->has('shipCity') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipCity')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="form-group">
                        {!! Form::label('shipZip', 'Postcode') !!}
                        {!! Form::text('shipZip', isset($order) ? $order->ship->zip : null, $attributes = $errors->has('shipZip') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('shipZip')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-1">
                    <div class="form-group">
                        {!! Form::label('shipCountry', 'Country') !!}
                        {!! Form::text('shipCountry', 'UK', $attributes = $errors->has('shipCountry') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control', 'disabled')) !!}
                    </div>
                </div>

            </div>
        </div>

        <div id="billing-details">
            <div class="row mb-3">
                <div class="col">
                    <h4 class="font-weight-bold">Billing Details</h4>
                </div>
            </div>

            <div class="row my-3">
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('billFirst', 'Firstname') !!}
                        {!! Form::text('billFirst', isset($order) ? $order->bill->firstname : null, $attributes = $errors->has('billFirst') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billFirst')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('billLast', 'Lastname') !!}
                        {!! Form::text('billLast', isset($order) ? $order->bill->lastname : null, $attributes = $errors->has('billLast') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billLast')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('billEmail', 'Email') !!}
                        {!! Form::text('billEmail', isset($order) ? $order->bill->email : null, $attributes = $errors->has('billEmail') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billEmail')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('billPhone', 'Phone') !!}
                        {!! Form::text('billPhone', isset($order) ? $order->bill->phone : null, $attributes = $errors->has('billPhone') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billPhone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="row my-3">
                <div class="col-sm-6 col-md-3">
                    <div class="form-group">
                        {!! Form::label('billState', 'State') !!}
                        {!! Form::text('billState', isset($order) ? $order->bill->state : null, $attributes = $errors->has('billState') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billState')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('billAddress1', 'Address 1') !!}
                        {!! Form::text('billAddress1', isset($order) ? $order->bill->address1 : null, $attributes = $errors->has('billAddress1') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billAddress1')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="form-group">
                        {!! Form::label('billAddress2', 'Address 2') !!}
                        {!! Form::text('billAddress2', isset($order) ? $order->bill->address2 : null, $attributes = $errors->has('billAddress2') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billAddress2')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="form-group">
                        {!! Form::label('billCity', 'City') !!}
                        {!! Form::text('billCity', isset($order) ? $order->bill->city : null, $attributes = $errors->has('billCity') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billCity')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="form-group">
                        {!! Form::label('billZip', 'Postcode') !!}
                        {!! Form::text('billZip', isset($order) ? $order->bill->zip : null, $attributes = $errors->has('billZip') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control')) !!}

                        @error('billZip')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6 col-md-1">
                    <div class="form-group">
                        {!! Form::label('shipCountry', 'Country') !!}
                        {!! Form::text('shipCountry', 'UK', $attributes = $errors->has('shipCountry') ? array('class' => 'form-control is-invalid') : array('class' => 'form-control', 'disabled')) !!}
                    </div>
                </div>
            </div>
        </div>

        <div id="products">
            <div class="row mb-3">
                <div class="col">
                    <h4 class="font-weight-bold">Products</h4>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    {!! Form::label('products', 'Products') !!}
                    {!! Form::select('products[]', $allProducts, null, array('class' => 'basic-multiple w-100', 'multiple' => 'multiple')) !!}
                </div>
            </div>
        </div>

    </div>

    <input type="hidden" name="on" id="on" value="true">

    <div class="card-footer text-right">
        {!! Form::submit('Save Order', ['class' => 'btn btn-purple']) !!}
    </div>

</div>
