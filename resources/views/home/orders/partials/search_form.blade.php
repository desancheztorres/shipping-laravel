<div class="row p-5">
    <div class="col-md-12">

        <div class="row">
            <div class="col-lg-3 col-md-12">

                {{-- From Input --}}
                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('from', 'From', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::date('from', isset($_GET['from']) ? date('Y-m-d', strtotime(request()->get('from'))) : \Carbon\Carbon::now(), array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- To Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('to', 'To', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::date('to', isset($_GET['to']) ? date('Y-m-d', strtotime(request()->get('to'))) : \Carbon\Carbon::now(), array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Order ID Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('orderID', 'Order ID:', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('orderID', isset($_GET['orderID']) ? request()->get('orderID') : '', array('class' => 'form-control', 'placeholder' => 'CSV of IDs')) !!}
                    </div>
                </div>

                {{-- Tracking ID Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('trackingNumber', 'Tracking #', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('trackingNumber', isset($_GET['trackingNumber']) ? request()->get('trackingNumber') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                {{-- Ship First Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipFirst', 'Firstname', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipFirst', isset($_GET['shipFirst']) ? request()->get('shipFirst') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Ship Last Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipLast', 'Lastname', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipLast', isset($_GET['shipLast']) ? request()->get('shipLast') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Ship Email Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipEmail', 'Email', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipEmail', isset($_GET['shipEmail']) ? request()->get('shipEmail') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Ship Phone Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipPhone', 'Phone', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipPhone', isset($_GET['shipPhone']) ? request()->get('shipPhone') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                {{-- Ship Address 1 Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipAddress1', 'Address 1', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipAddress1', isset($_GET['shipAddress1']) ? request()->get('shipAddress1') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Ship Address 2 Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipAddress2', 'Address 2', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipAddress2', isset($_GET['shipAddress2']) ? request()->get('shipAddress2') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Ship State Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipState', 'State', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipState', isset($_GET['shipState']) ? request()->get('shipState') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Ship City Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipCity', 'City', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipCity', isset($_GET['shipCity']) ? request()->get('shipCity') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                {{-- Ship Zip Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('shipZip', 'Zip', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::text('shipZip', isset($_GET['shipZip']) ? request()->get('shipZip') : '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                {{-- Products Input --}}

                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-3">
                        <div class="form-group row">
                            {!! Form::label('products', 'Products', ['class' => 'mr-2 font-weight-bold']) !!}
                        </div>

                    </div>
                    <div class="col-sm-12 col-lg-9">
                        {!! Form::select('products[]', $allProducts, null, array('class' => 'basic-multiple w-100', 'multiple' => 'multiple')) !!}
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

{!! Form::submit('Search', ['class' => 'btn btn-purple']) !!}
