<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Shipping') }}</title>

    <!-- Dashboard css -->
    <link href="{{ asset('assets/css/dashboard.css') }}" rel="stylesheet" />

    <!-- Font family-->
    <link href="//fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

    <!-- C3 Charts css-->
    <link href="{{ asset('assets/plugins/charts-c3/c3-chart.css') }}" rel="stylesheet" />

    <!-- Custom scroll bar css-->
    <link href="{{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet" />

    <!-- Sidemenu css -->
    <link href="{{ asset('assets/plugins/toggle-sidebar/sidemenu.css') }}" rel="stylesheet" />

    @yield('css')

    <!---Font icons css-->
    <link href="{{ asset('assets/plugins/iconfonts/plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/fonts/fonts/font-awesome.min.css') }}" rel="stylesheet">

</head>
<body class="@yield('body_class')">

<!--Global-Loader-->
<div id="global-loader"></div>

<div class="page">
    @yield('main_content')
</div>
<!-- End Page -->

    <!-- Back to top -->
    <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Jquery js-->
    <script src="{{ asset('assets/js/vendors/jquery-3.2.1.min.js') }}"></script>

    <!--Bootstrap js-->
    <script src="{{ asset('assets/js/vendors/bootstrap.bundle.min.js') }}"></script>

    <!--Jquery Sparkline js-->
    <script src="{{ asset('assets/js/vendors/jquery.sparkline.min.js') }}"></script>

    <!-- Chart Circle js-->
    <script src="{{ asset('assets/js/vendors/circle-progress.min.js') }}"></script>

    <!-- Star Rating js-->
    <script src="{{ asset('assets/plugins/rating/jquery.rating-stars.js') }}"></script>

    <!--Side-menu js-->
    <script src="{{ asset('assets/plugins/toggle-sidebar/sidemenu.js') }}"></script>

    <!-- Custom scroll bar js-->
    <script src="{{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    @yield('js')

    <!-- Custom js-->
    <script src="{{ asset('assets/js/custom.js') }}"></script>

</body>
</html>
