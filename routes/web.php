<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::view('/', 'auth.login')->middleware('guest');


Route::get('logout', 'Auth\LoginController@logout');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
});

/**
 * Orders
 */
Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
    Route::get('/', 'OrderController@index')->name('index');
    Route::post('/', 'OrderController@store')->name('store');
    Route::get('/create', 'OrderController@create')->name('create');
    Route::get('/dispatch', 'OrderController@dispatchOrder')->name('dispatchOrder');
    Route::get('/sync', 'OrderController@syncOrder')->name('syncOrder');
    Route::get('/merge', 'OrderController@mergeOrder')->name('mergeOrder');
    Route::get('/{status}', 'OrderController@index')->name('index');
    Route::get('/{order}/edit', 'OrderController@edit')->name('edit');
    Route::put('/{order}', 'OrderController@update')->name('update');
    Route::delete('/{order}', 'OrderController@destroy')->name('delete');
});
